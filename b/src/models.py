from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Float, DateTime
from sqlalchemy.orm import relationship

from database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)
    consumptions = relationship("Consumption", back_populates="user")


class Noodle(Base):
    __tablename__ = "noodles"

    id = Column(Integer, primary_key=True, index=True)
    brand = Column(String)
    flavour = Column(String)
    consumptions = relationship("Consumption", back_populates="noodle")


class Consumption(Base):
    __tablename__ = "consumptions"

    id = Column(Integer, primary_key=True, index=True)
    amount = Column(Float)
    timestamp = Column(DateTime)
    noodle_id = Column(Integer, ForeignKey("noodles.id"))
    user_id = Column(Integer, ForeignKey("users.id"))
    noodle = relationship("Noodle")
    user = relationship("User")
