from sqlalchemy.orm import Session

from datetime import datetime
import models
import schemas


def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate):
    # TODO: change to encrypted password
    fake_hashed_password = user.password
    db_user = models.User(email=user.email, hashed_password=fake_hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def add_consumption(db: Session, user_id: int, noodle_id: int, amount: float):
    db_consumption = models.Consumption(
        user_id=user_id, noodle_id=noodle_id, amount=amount, timestamp=datetime.now()
    )
    db.add(db_consumption)
    db.commit()
    db.refresh(db_consumption)
    return db_consumption.timestamp
