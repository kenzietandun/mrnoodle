from typing import List

from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

import crud
import models
import schemas
from database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/users", response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return crud.create_user(db=db, user=user)


def verify_password(db_password, request_password):
    # TODO: change to encrypted password
    return db_password == request_password


@app.post("/login", response_model=schemas.User)
def login(user: schemas.UserLogin, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if not db_user:
        raise HTTPException(status_code=400, detail="Invalid credentials")
    if not verify_password(db_user.hashed_password, user.password):
        raise HTTPException(status_code=400, detail="Invalid credentials")
    return db_user


@app.post("/consume/{user_id}")
def add_consumption(
    user_id: int, noodle_intake: schemas.NoodleIntake, db: Session = Depends(get_db)
):
    crud.add_consumption(db, user_id, noodle_intake.id, noodle_intake.amount)
